import React, { PureComponent } from 'react';
import { withSiteData } from 'react-static';

import Navigation, { Link } from 'components/Navigation';
import { Wrap, ContentWrap } from './Dashboard.style';

class Dashboard extends PureComponent {
  render() {
    const { routes } = this.props;

    return (
      <Wrap>
        <Navigation>
          {routes.map(
            route =>
              route.inNavigation && (
                <Link key={route.path} to={route.path}>
                  {route.name}
                </Link>
              ),
          )}
        </Navigation>
        <ContentWrap>{this.props.children}</ContentWrap>
      </Wrap>
    );
  }
}

export default withSiteData(Dashboard);
