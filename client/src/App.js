import React from 'react';
import { Router } from 'react-static';
import { ThemeProvider, injectGlobal } from 'styled-components';
import normalize from 'styled-normalize';
import { hot } from 'react-hot-loader';

// import { Provider } from 'react-redux';
// import store from 'redux/config';

import { ApolloProvider } from 'react-apollo';
import client from 'apollo/client';

import { theme } from 'config';

import Routes from 'react-static-routes';

injectGlobal`
  ${normalize};

  html, body {
    font-family: 'Monsterrat', sans-serif;
  }

  * {
    box-sizing: border-box;
  }
`;

const App = () => (
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <Router>
        <Routes />
      </Router>
    </ThemeProvider>
  </ApolloProvider>
);

export default hot(module)(App);
