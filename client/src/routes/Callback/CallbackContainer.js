import { PureComponent } from 'react';
import { compose, withApollo } from 'react-apollo';
// import { compose } from 'redux';
// import { connect } from 'react-redux';
import { withSiteData, withRouter } from 'react-static';
import qs from 'query-string';
// import { callbackSuccess } from 'redux/auth/auth.actions';
import { GET_TOKENS, SET_TOKENS } from 'queries/auth.gql';

class CallbackContainer extends PureComponent {
  componentDidMount() {
    this.getTokens();
  }

  getTokens = async () => {
    const { client, history } = this.props;
    const params = qs.parse(window.location.search);

    const {
      data: { tokens },
    } = await client.query({
      query: GET_TOKENS,
      variables: {
        code: params.code,
      },
    });

    await client.mutate({
      mutation: SET_TOKENS,
      variables: {
        accessToken: tokens.accessToken,
        refreshToken: tokens.refreshToken,
      },
    });

    history.push('/');
  };

  render() {
    return null;
  }
}

// const mapDispatchToProps = {
//   dispatchCallbackSuccess: callbackSuccess,
// };

// const enhancer = compose(
//   connect(
//     null,
//     mapDispatchToProps,
//   ),
//   withSiteData,
// );

const enhancer = compose(
  withSiteData,
  withRouter,
  withApollo,
);

export default enhancer(CallbackContainer);
