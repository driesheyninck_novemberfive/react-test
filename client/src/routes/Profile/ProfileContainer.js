import React from 'react';
import { compose } from 'react-apollo';
import { withSiteData } from 'react-static';
// import { compose } from 'redux';
// import { connect } from 'react-redux';
// import { getCallbackCode } from 'redux/auth/auth.selectors';

import Dashboard from 'components/Dashboard';
import Profile from './Profile';

const ProfileContainer = props => (
  <Dashboard>
    <Profile {...props} />
  </Dashboard>
);

// const mapStateToProps = state => ({
//   code: getCallbackCode(state),
// });

// const enhancer = compose(
//   withSiteData,
//   connect(mapStateToProps),
// );

const enhancer = compose(withSiteData /* TODO: graphql... */);

export default enhancer(ProfileContainer);
