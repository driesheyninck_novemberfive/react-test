import React, { PureComponent } from 'react';

class Home extends PureComponent {
  componentDidMount() {
    console.info('hello');
  }

  render() {
    return (
      <div>
        <h1>Home Component</h1>
      </div>
    );
  }
}

export default Home;
