import React from 'react';
import { compose } from 'redux';
import { withSiteData } from 'react-static';
// import { connect } from 'react-redux';
// import { getCallbackCode } from 'redux/auth/auth.selectors';

import Dashboard from 'components/Dashboard';
import Home from './Home';

const HomeContainer = props => (
  <Dashboard>
    <Home {...props} />
  </Dashboard>
);

// const mapStateToProps = state => ({
//   code: getCallbackCode(state),
// });

// const enhancer = compose(
//   withSiteData,
//   connect(mapStateToProps),
// );

const enhancer = compose(withSiteData);

export default enhancer(HomeContainer);
