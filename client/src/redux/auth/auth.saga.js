import { takeLatest } from 'redux-saga/effects';

import { CALLBACK_SUCCESS } from './auth.actions';

function* fetchTokensFlow(action) {
  // TODO....
  // 1) call http://localhost:8080/callback?code={YOUR_CODE}
  // 2) get the tokens
  // 3) put the tokens in the store
  // 4) redirect to home
}

function* authSaga() {
  yield takeLatest(CALLBACK_SUCCESS, fetchTokensFlow);
}

export default authSaga;
