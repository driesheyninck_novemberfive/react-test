import { CALLBACK_SUCCESS } from './auth.actions';

const INITIAL_STATE = {
  code: null,
  accessToken: null,
  refreshToken: null,
};

export default function authReducer(state = INITIAL_STATE, action) {
  if (!action.type || !action.payload) {
    return state;
  }

  switch (action.type) {
    default:
      return state;
  }
}
