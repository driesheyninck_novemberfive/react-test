/**
 * State selectors
 *
 */

export const getCallbackCode = state => state && state.auth && state.auth.code;
