export const CALLBACK_SUCCESS = 'CALLBACK_SUCCESS';

export function callbackSuccess(code) {
  return {
    type: CALLBACK_SUCCESS,
    payload: {
      code,
    },
  };
}
