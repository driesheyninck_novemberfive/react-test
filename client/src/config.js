export const API_BASE = 'http://localhost:8080';

export const theme = {
  palette: {
    global01: '#1DB954',
    neutral01: '#FFFFFF',
    neutral02: '#191414',
  },

  sizes: {
    navigationHeight: 72,
  },
};
