import apiManager from './apiManager';

export function example() {
  return apiManager('/me').then(res => res);
}
