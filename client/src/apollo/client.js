import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';

import { stateLink, restLink, authLink } from './links';

const cache = new InMemoryCache();

const client = new ApolloClient({
  link: ApolloLink.from([authLink, stateLink(cache), restLink]),
  cache,
});

export default client;
