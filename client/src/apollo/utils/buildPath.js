// @flow

function buildQueryParamsPath({ path, variables }) {
  const qs = Object.keys(variables).reduce(
    (acc: string, key: string): string => {
      if (variables[key] === null || variables[key] === undefined) {
        return acc;
      }

      if (acc === '') {
        return `${key}=${encodeURIComponent(String(variables[key]))}`;
      }

      return `${acc}&${key}=${encodeURIComponent(String(variables[key]))}`;
    },
    // The acc is a string in this reduce function.
    '',
  );

  return `/${path}?${qs}`;
}

export default buildQueryParamsPath;
