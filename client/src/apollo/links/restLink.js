import { RestLink } from 'apollo-link-rest';
import humps from 'humps';
import fetchHeaders from 'fetch-headers/headers-es5.min';
import { API_BASE } from 'config';

// polyfill for undefined headers with apollo-link-rest
// https://github.com/apollographql/apollo-link-rest/issues/41
global.Headers = global.Headers || fetchHeaders;

// setup your Rest link with your endpoint
const restLink = new RestLink({
  uri: API_BASE,
  credentials: 'same-origin',
  fieldNameNormalizer: key => humps.camelize(key),
  fieldNameDenormalizer: key => humps.decamelize(key),
});

export default restLink;
