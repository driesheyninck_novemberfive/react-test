import { setContext } from 'apollo-link-context';
import { GET_AUTH_TOKEN } from 'queries/auth.gql';

const authLink = setContext(async (request, { cache: localCache }) => {
  const {
    auth: { accesToken },
  } = localCache.read({
    query: GET_AUTH_TOKEN,
  });

  return {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: accesToken ? `Bearer ${accesToken}` : '',
    },
  };
});

export default authLink;
