import { withClientState } from 'apollo-link-state';
import { Mutation, initialState } from '../state';

// local state
const stateLink = cache =>
  withClientState({
    cache,
    // retrieve & update local state
    resolvers: {
      Mutation,
    },
    // default (initial) state
    defaults: initialState,
  });

export default stateLink;
