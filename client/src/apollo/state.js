export const Mutation = {
  updateTokens: (_, { accessToken, refreshToken }, { cache: localCache }) => {
    const data = {
      auth: {
        __typename: 'Auth',
        accessToken,
        refreshToken,
      },
    };
    localCache.writeData({ data });
    return null;
  },
};

export const initialState = {
  auth: {
    __typename: 'Auth',
    accessToken: null,
    refreshToken: null,
  },
};
